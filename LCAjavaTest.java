import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

//-------------------------------------------------------------------------
/**
 *  Test class for Doubly Linked List
 *
 *  @version 3.1 09/11/15 11:32:15
 *
 *  @author  Emmet McDonald
 */

@RunWith(JUnit4.class)
public class LCAjavaTest
{
  
  @Test
  public void testContainsBST() {
	  LCAjava<Integer> bst = new LCAjava<Integer>();
	  
      bst.put(7);   //        _7_
      bst.put(8);   //      /     \
      bst.put(3);   //    _3_      8
      bst.put(1);   //  /     \
      bst.put(2);   // 1       6
      bst.put(6);   //  \     /
      bst.put(4);   //   2   4
      bst.put(5);   //        \

     
      assertTrue("Contains valid key", bst.contains(6));
      assertFalse("contains invalid key", bst.contains(9));
  }
  
  @Test
  public void BSTtestLCA() {
	  LCAjava<Integer> bst = new LCAjava<Integer>();
	  
	  assertEquals("Invalid BST", bst.lca(2, 4), null);
	  
      bst.put(7);   //        _7_
      bst.put(8);   //      /     \
      bst.put(3);   //    _3_      8
      bst.put(1);   //  /     \
      bst.put(2);   // 1       6
      bst.put(6);   //  \     /
      bst.put(4);   //   2   4
      bst.put(5);   //        \
      
      assertEquals("Invalid test Keys", bst.lca(25, 0), null);
      assertEquals("One invalid, one valid", bst.lca(2, 25), null);
      
      assertEquals("Valid test keys - root is LCA", (bst.lca(2, 8)).get(0).key, (Integer) 7);
      assertEquals("Valid test keys - root is LCA", (bst.lca(2, 8)).size(), 1);
      
      assertEquals("Valid test keys - something else is LCA", (bst.lca(2, 4)).get(0).key, (Integer) 3);
      assertEquals("Valid test keys - something else is LCA", (bst.lca(2, 4)).size(), 1);

      assertEquals("Valid Test Keys - one more for good measure", (bst.lca(4, 5)).get(0).key, (Integer) 4);
      assertEquals("Valid Test Keys - one more for good measure", (bst.lca(4, 5)).size(), 1);
	  
  }
  
  @Test
  public void testContainsDAG() {
	  System.out.println("DOING DAG NOW");
	  LCAjava<Character> dag = new LCAjava<Character>();
	  
	  dag.put('a');
	  dag.put('b', new Character[] {'a'});
	  dag.put('c', new Character[] {'a'});
	  dag.put('d', new Character[] {'a','b','c'});
	  dag.put('e', new Character[] {'a','c','d'});
	  
	  assertTrue("Contains valid key", dag.contains('e'));
	  assertFalse("contains invalid key", dag.contains('q'));	  
  }
  
  @Test
  public void DAGtestLCA() {
	  LCAjava<Character> dag = new LCAjava<Character>();
	  
	  dag.put('a');
	  dag.put('b', new Character[] {'a'});
	  dag.put('c', new Character[] {'a'});
	  dag.put('d', new Character[] {'a','b','c'});
	  dag.put('e', new Character[] {'a','c','d'});
	  
      assertEquals("Invalid test Keys", dag.lca('x', 'y'), null);
      assertEquals("One invalid, one valid", dag.lca('d', 'y'), null);
      assertEquals("Valid test keys - root is LCA", (dag.lca('b', 'c')).get(0).key, (Character) 'a');
      assertEquals("Valid test keys - root is LCA", (dag.lca('b', 'c')).size(), 1);
      
      LCAjava<Character> subtree = new LCAjava<Character>();
      subtree.put('a');
      subtree.put('c', new Character[] {'a'});
      
      assertEquals("Valid test keys - Another LCA", (dag.lca('d', 'e')).size(), 1);
      assertEquals("Valid test keys - Another LCA", (dag.lca('d', 'e')).get(0).key, (Character) 'd');
  }
     
}
