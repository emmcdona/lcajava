import java.util.ArrayList;
import java.util.Collections;

public class LCAjava<Key extends Comparable<Key>> {
    public Node root;             // root of BST
    /**
     * Private node class.
     */
    class Node {
        public Key key;           // sorted by key
        private ArrayList<Node> children = new ArrayList<Node>();
        private Node left, right;  // left and right subtrees

        public Node(Key key) {
            this.key = key;
        }
    }

    public boolean contains(Key key) {
    	return contains(root, key) != null;
    }

   
    private Node contains(Node x, Key key) {
    	if(x == null) return null;
    	else if(x.key == key) return x;
    	else {
    		for(Node child:x.children) {
    			Node node = contains(child, key);
    			if (node != null) return node;
    		}
    		return null;
    	}
    }

    public void put(Key key) {
        root = put(root, key);
    }

    private Node put(Node x, Key key) {
        if (x == null) return new Node(key);
        int cmp = key.compareTo(x.key);
        if (cmp < 0) {
        	x.left  = put(x.left,  key);
        	(x.children).add(x.left);
        }
        else if (cmp > 0) {
        	x.right = put(x.right, key);
        	(x.children).add(x.right);
        }
        return x;
    }
    
    public void put(Key key, Key[] parents) {
    	if(!contains(key)) {
    		Node node = new Node(key);
    		for(Key parent: parents) {
    			Node pNode = contains(root, parent);
    			(pNode.children).add(node);
    		}
    	}
    }
    
    
    public ArrayList<Node> lca(Key n1, Key n2) {
    	if(contains(n1) && contains(n2)) {
    		Node t1 = contains(root, n1);
    		Node t2 = contains(root, n2);
        	ArrayList<Node> cParents = new ArrayList<Node>();
        	System.out.println("Checking " + root);
    		lca(root, n1, n2, cParents);
    		ArrayList<Integer> dists = new ArrayList<Integer>();
    		for(Node parent: cParents) {
    			dists.add(dist(parent, t1) + dist(parent, t2));
    		}
    		int lowest = Collections.min(dists);
    		ArrayList<Node> LCAs = new ArrayList<Node>();
    		for(int i = 0; i < dists.size(); i++) {
    			if(dists.get(i) == lowest) {
    				LCAs.add(cParents.get(i));
    			}
    		}
    		
    		System.out.println("LCAs[0]: " + LCAs.get(0).key);
    		return LCAs;
    	}
    	else {
    		return null;
    	}
    }
    
    private void lca(Node x, Key n1, Key n2, ArrayList<Node> parents) {
    	if((contains(x, n1) != null) && (contains(x, n2) != null) && !parents.contains(x)) {
    		parents.add(x);
    	}
    	for(Node child : x.children) {
    		lca(child, n1, n2, parents);
    	}
	}
    
    private int dist(Node top, Node target) {
    	if(top.equals(target)) {
    		return 0;
    	}
    	if(top.children.contains(target)) {
    		return 1;
    	}
    	if(top.children.size() == 0) {
    		return 9999;
    	}
    	else {
			ArrayList<Integer> vals = new ArrayList<Integer>();
    		for(Node child:top.children) {
    			vals.add(1 + dist(child, target));
    		}
    		return  Collections.min(vals);
    	}
    }
}